set _cd_ [ file normalize [ file dir [ info script ] ] ]
lappend auto_path [ file join $_cd_ lib ]

source [ file join $_cd_ "TolClient.tcl" ]

set tolDir [ string trim [ lindex [ tol::info var { Text TolAppDataPath } ] 2 ] \" ]
set clientDir [ file join $tolDir client ]
file mkdir $clientDir
set clientLog [ file join $clientDir "tolcomm.cli.[pid].log" ]
::tolclient::init $clientLog